import flask
from flask import Flask, send_from_directory, request, jsonify, redirect, render_template, url_for, flash
import json
import hashlib

app = Flask(__name__)

file_path = 'discovered_combinations.json'

try:
    with open('possible_combinations.json', 'r') as file:
        possible_combinations = json.load(file)
    pair_lookup = {(pair[0], pair[1]): {'result': entry['result'], 'emoji': entry['emoji']} for entry in possible_combinations for pair in [entry['pair']]}
    discovered_objects = set()
except FileNotFoundError:
    pass

try:
    with open("discovered_combinations.json", 'r') as file:
        discovered_objects = set(json.load(file))
except FileNotFoundError:
    pass

try:
    with open('possible_combinations.json', 'r') as file:
        initial_data = json.load(file)
    initial_data_formatted = json.dumps(initial_data, indent=4)
except FileNotFoundError:
    pass

with open('password.txt', 'r', encoding='utf-8') as password_file:
    HASHED_PASSWORD = password_file.read().strip()

@app.route('/')
def index():
    return send_from_directory('', 'index.html')

@app.route('/infinite-craft/')
def index2():
    return send_from_directory('neal', 'index.html')

@app.route('/<path:path>')
def everything(path):
    return send_from_directory('neal', path)

@app.route('/api/infinite-craft/pair', methods=['GET'])
def get_pair():
    with open('possible_combinations.json', 'r') as file:
        possible_combinations = json.load(file)
    pair_lookup = {(pair[0], pair[1]): {'result': entry['result'], 'emoji': entry['emoji']} for entry in possible_combinations for pair in [entry['pair']]}
    discovered_objects = set()
    try:
        with open(file_path, 'r') as file:
            discovered_objects = set(json.load(file))
    except FileNotFoundError:
        pass
    first = request.args.get('first')
    second = request.args.get('second')

    # Check if both 'first' and 'second' parameters are provided
    if not first or not second:
        return jsonify({'error': 'Both "first" and "second" parameters are required.'}), 400

    # Look up the result for the pair
    pair_key = (first, second)
    if pair_key in pair_lookup:
        pair_result = pair_lookup[pair_key]
        # Check if the object is discovered for the first time
        isNew = pair_result['result'] not in discovered_objects
        pair_result['isNew'] = isNew
        # Add the discovered object to the set and save to JSON file
        if isNew:
            discovered_objects.add(pair_result['result'])
            with open(file_path, 'w') as file:
                json.dump(list(discovered_objects), file)
        return jsonify(pair_result)
    else:
        # If no result found for the pair, return the default response
        default_result = {'result': 'Nothing', 'emoji': ''}
        return jsonify(default_result)

@app.route('/admin')
def admin_customcraft():
    with open('possible_combinations.json', 'r') as file:
        initial_data = json.load(file)
    initial_data_formatted = json.dumps(initial_data, indent=4, ensure_ascii=False)

    # Pass the data to the template
    return render_template('admin/customcraft.html', data=initial_data_formatted)

@app.route('/admin/submit', methods=['POST'])
def admin_customcraft_submit():
    if request.method == 'POST':
        entered_password = request.form.get('password')
        # Hash the entered password
        hashed_entered_password = hashlib.sha256(entered_password.encode()).hexdigest()
        if hashed_entered_password == HASHED_PASSWORD:
            # Handle form data
            pair1 = request.form.get('pair 1')
            pair2 = request.form.get('pair 2')
            result = request.form.get('result')
            emoji = request.form.get('emoji')

            # Update data
            new_combination = {
                "pair": [pair1, pair2],
                "result": result,
                "emoji": emoji
            }
            initial_data.append(new_combination)

            # Save updated data
            with open('possible_combinations.json', 'w', encoding='utf-8') as file:
                json.dump(initial_data, file, indent=4, ensure_ascii=False)

            flash('Combination added successfully!', 'success')
        else:
            flash('Incorrect password!', 'error')
    return redirect(url_for('admin_customcraft'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
