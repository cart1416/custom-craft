# Getting started

Clone this repo, install flask and run the flask_app.py

```
pip install Flask
git clone https://gitlab.com/cart1416/custom-craft.git
cd custom-craft
python3 flask_app.py
```
you may want to replace the password.txt with your sha256 encrypted password to get the admin panel working
go to the admin panel on <address>/admin